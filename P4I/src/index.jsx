import React from 'react'
import ReactDOM from 'react-dom/client'
import Browser from './Browser'


ReactDOM.createRoot(document.getElementById('root')).render(
	<React.StrictMode>
		<Browser />
	</React.StrictMode>
)